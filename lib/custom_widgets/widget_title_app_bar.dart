import 'dart:io';

import 'package:flutter/material.dart';

PreferredSizeWidget widgetTitleAppBar({required String textTitle}) => AppBar(
      title: Container(
        child: Platform.isAndroid
            ? Center(
                child: Padding(
                  padding: const EdgeInsets.only(right: 50),
                  child: Container(
                    child: widgetTitleTextAppBar(textTitle: textTitle),
                  ),
                ),
              )
            : widgetTitleTextAppBar(textTitle: textTitle),
      ),
      backgroundColor: Colors.white,
      elevation: 0,
      iconTheme: IconThemeData(color: Colors.black),
    );

Widget widgetTitleTextAppBar({required String textTitle}) => Text(
      textTitle,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        color: Colors.black,
        letterSpacing: 2,
        fontWeight: FontWeight.bold,
      ),
    );

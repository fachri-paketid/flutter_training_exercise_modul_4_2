import 'package:flutter/material.dart';
import 'package:flutter_app_third/blog_detail_page.dart';
import 'package:flutter_app_third/blog_page.dart';

Widget widgetBlog({
  required dynamic context,
  required BlogData blogData,
}) =>
    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => BlogDetailPage(blogData: blogData),
              ),
            );
          },
          borderRadius: BorderRadius.circular(20),
          child: Container(
            child: Image.asset(blogData.image, fit: BoxFit.cover),
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.1),
              borderRadius: BorderRadius.circular(20),
            ),
            height: 200,
            width: double.infinity,
          ),
        ),
        SizedBox(height: 18),
        InkWell(
          onTap: () {
            print(blogData.title);
          },
          borderRadius: BorderRadius.circular(10),
          child: Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  blogData.title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Text(
                  blogData.subtitle,
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          width: 50,
          child: Divider(
            color: Colors.black,
            thickness: 5,
          ),
        ),
        SizedBox(height: 20),
      ],
    );

import 'package:flutter/material.dart';
import 'custom_widgets/widget_title_app_bar.dart';
import 'custom_widgets/widget_blog.dart';

class BlogData {
  String image;
  String title;
  String subtitle;

  BlogData({required this.image, required this.title, required this.subtitle});
}

List<BlogData> blogDataList = [
  BlogData(
    image: "images/rich.png",
    title: "Top 10 tips to retired at 40 years old",
    subtitle: "The best tips to retired early",
  ),
  BlogData(
    image: "images/house.png",
    title: "How to purcase a property in 5 steps",
    subtitle: "You must aknowledge this before shopping",
  ),
  BlogData(
    image: "images/apps.png",
    title: "7 apps to increase productivity",
    subtitle: "These apps will boost your daily tasks",
  ),
];

class BlogPage extends StatelessWidget {
  const BlogPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: widgetTitleAppBar(textTitle: "The Blog"),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ...List.generate(
              blogDataList.length,
              (index) => widgetBlog(
                context: context,
                blogData: blogDataList[index],
              ),
            ),
            TextButton(
              onPressed: () {
                print("Terms and Conditions");
              },
              child: Text("Terms and Conditions"),
              style: TextButton.styleFrom(
                primary: Colors.blueGrey[900],
                minimumSize: Size(double.infinity, 45),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Sign Out"),
              style: TextButton.styleFrom(
                primary: Colors.red,
                minimumSize: Size(double.infinity, 45),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
